;(package-initialize)
(let ((file-name-handler-alist nil))
  (setq gc-cons-threshold 5000000)

(setq straight-repository-branch "develop")
(setq straight-recipes-gnu-elpa-use-mirror t)
(setq straight-check-for-modifications 'live)
(let ((bootstrap-file (concat user-emacs-directory "straight/repos/straight.el/bootstrap.el"))
      (bootstrap-version 3))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq user-full-name "Rev. Elijah Schwab, O. Carm."
      user-mail-address "elijahschwab@gmail.com")

;(add-to-list 'custom-theme-load-path "~/.emacs.d/cyberpunk/")
(straight-use-package 'cyberpunk-theme)
(load-theme 'cyberpunk t)
(eval-after-load "org"
  '(progn
     (set-face-background 'org-block "#000000")
     (set-face-background 'org-block-begin-line "#000000")
     (set-face-background 'org-block-end-line "#000000")
     (set-face-background 'org-column "#000000")
     (set-face-attribute 'org-level-1 nil :height 1.0)
     (set-face-attribute 'org-level-2 nil :height 1.0)
     (set-face-attribute 'org-level-3 nil :height 1.0)
     ))
(set-face-foreground 'mode-line "#dd5608")
(set-face-background 'mode-line "#111111")
(set-face-attribute  'mode-line nil :box '(:line-width -1 :color "#dd5608"))
(set-face-background 'mode-line-inactive "#111111")

;  (set-face-foreground 'line-number "#98c2c7")
;  See Hooks for LaTeX mode and prog-mode for linum face.

(set-default 'cursor-type 'bar)
(set-frame-font "Monospace-10")

(setq ring-bell-function
      (lambda () (message "*beep*"))
)

(global-visual-line-mode 1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)
(eval-after-load 'tramp '(setenv "SHELL" "/bin/zsh"))

(setq-default require-final-newline t)

(setq frame-title-format '("%b - " invocation-name "@" system-name))

(setq browse-url-browser-function 'browse-url-generic
      browse-url-generic-program "chromium")

(fset 'yes-or-no-p 'y-or-n-p)

(defun fast-move-down ()
  "Move point down 5 lines."
  (interactive)
  (ignore-errors (next-line 5)))

(defun fast-move-up ()
  "Move point up 5 lines."
  (interactive)
  (ignore-errors (previous-line 5)))

(global-set-key (kbd "M-n") 'fast-move-down)
(global-set-key (kbd "M-p") 'fast-move-up)

(setq make-backup-files nil)

(setq inhibit-startup-message t)
  (setq initial-scratch-message (purecopy "\
;; Scratch Buffer

"))

(require 'printing)
(pr-update-menus t)

(defvar lorem-ipsum-text
  "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
occaecat cupidatat non proident, sunt in culpa qui officia deserunt
mollit anim id est laborum."
  "The first paragraph of the Lorem Ipsum text")

(defun lorem-ipsum ()
  "Insert a Lorem ipsum text"
  (interactive)
  (insert lorem-ipsum-text))

(defun backlight-echo ()
  "Display the current value of the backlight brightness."
  (interactive)
  (message "Backlight Val: %s" 
           (string-to-number
            (with-temp-buffer
              (insert-file-contents
               "/sys/class/backlight/acpi_video0/brightness")
              (buffer-string)))))

(global-set-key (kbd "<XF86MonBrightnessUp>") 'backlight-echo)
(global-set-key (kbd "<XF86MonBrightnessDown>") 'backlight-echo)

(straight-use-package 'hydra)

(add-to-list 'auto-mode-alist
             '("\\(?:\\.\\(?:install\\|zsh\\(?:rc\\)?\\)\\|PKGBUILD\\)"
               . sh-mode))
(add-to-list 'auto-mode-alist '("\\php$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.gabc$" . gregorio-mode))

(straight-use-package 'iedit)
(define-key global-map (kbd "C-;") 'iedit-mode)

(defalias 'list-buffers 'ibuffer)
(setq ibuffer-saved-filter-groups
      '(("first"
         ("*buffers*" (name . "\\*.*\\*"))
         ("Dired" (mode . dired-mode))
         ("emacs-config" (filename . ".emacs.d\*"))
         ("Web Dev" (or (mode . html-mode)
                        (mode . css-mode)))
         ("GABC" (mode . gabc-mode))
         ("Latex" (mode . latex-mode))
         ("Gregorio-Dev" (filename . "Gregorio-Project/gregorio\*"))
         ("Bash" (mode . sh-mode))
         ("Python" (mode . python-mode))
         ("Org" (mode . org-mode))
)))
(defadvice ibuffer-generate-filter-groups (after reverse-ibuffer-groups ()
                                                  activate)
     (setq ad-return-value (nreverse ad-return-value)))
(setq ibuffer-show-empty-filter-groups nil
      ibuffer-filter-group-name-face 'font-lock-doc-face
      ibuffer-expert t
      ibuffer-display-summary nil)

(eval-after-load 'ibuffer
  '(progn
     ;; Use human readable Size column instead of original one
     (define-ibuffer-column size-h
       (:name "Size" :inline t)
       (cond
        ((> (buffer-size) 1000000) (format "%7.2fM" (/ (buffer-size) 1000000.0)))
        ((> (buffer-size) 1000) (format "%7.2fk" (/ (buffer-size) 1000.0)))
        (t (format "%8d" (buffer-size)))))))
(setq ibuffer-formats
      '((mark modified read-only " "
              (name 18 18 :left :elide)
              " "
              (size-h 8 -1 :right)
              " "
              (mode 16 16 :left :elide)
              " "
              (filename-and-process -17 30 :left :elide))))

(defun next-user-buffer ()
  "Switch to the next user buffer."
  (interactive)
  (next-buffer)
  (let ((i 0))
    (while (and (string-match "^*" (buffer-name)) (< i 50))
      (setq i (1+ i)) (next-buffer) )))
(defun previous-user-buffer ()
  "Switch to the previous user buffer."
  (interactive)
  (previous-buffer)
  (let ((i 0))
    (while (and (string-match "^*" (buffer-name)) (< i 50))
      (setq i (1+ i)) (previous-buffer) )))
(global-set-key (kbd "<C-prior>") 'previous-user-buffer)
(global-set-key (kbd "<C-next>") 'next-user-buffer)

(ido-mode t)
(ido-everywhere t)
(setq ido-ignore-buffers '("\\*.*\\*"))
(setq ido-file-extensions-order '(".org" ".conf" ".py" ".gabc" ".tex" ".pdf"))

(straight-use-package
 '(dired+ :type git :host github :repo "emacsmirror/dired-plus"))

(eval-after-load "dired+"
  '(progn
     (setq dired-listing-switches "-alho")
     (put 'dired-find-alternate-file 'disabled nil)
     (set-face-foreground 'diredp-dir-heading "Green")
     (set-face-background 'diredp-dir-heading nil)
     (set-face-background 'diredp-write-priv nil)
     (set-face-background 'diredp-read-priv nil)
     (set-face-background 'diredp-no-priv nil)
     (set-face-background 'diredp-exec-priv nil)
     (set-face-background 'diredp-dir-priv nil)
     (defun dired-back-to-top ()
       "Move point to the first file of the directory."
       (interactive)
       (beginning-of-buffer)
       (dired-next-line 4))
     (defun dired-jump-to-bottom ()
       "Move point to the last file of the directory."
       (interactive)
       (end-of-buffer)
       (dired-next-line -1))
     (define-key dired-mode-map
       (vector 'remap 'beginning-of-buffer) 'dired-back-to-top)
     (define-key dired-mode-map
       (vector 'remap 'end-of-buffer) 'dired-jump-to-bottom)
     (defun dired-do-find-marked-files-custom ()
       "Find all marked files opening all of them simultaneously without
     visiting them. This function is made to overwrite
     dired-do-find-marked-files from dired+.el"
       (interactive)
       (dired-simultaneous-find-file (dired-get-marked-files) 4))
     (define-key dired-mode-map
       (vector 'remap 'dired-do-find-marked-files) 'dired-do-find-marked-files-custom)
     ))

(straight-use-package 'magit)
(straight-use-package 'magit-section)

; Straight should add these dependencies automatically
(add-to-list 'load-path "/home/elijah/.emacs.d/straight/build/with-editor")
(add-to-list 'load-path "/home/elijah/.emacs.d/straight/build/git-commit")
(add-to-list 'load-path "/home/elijah/.emacs.d/straight/build/transient")

(global-set-key (kbd "<f5> s") 'magit-status)
(global-set-key (kbd "<f5> r") 'magit-list-repositories)

(with-eval-after-load 'info
  (info-initialize)
  (add-to-list 'Info-directory-list
               "~/.emacs.d/straight/repos/magit/Documentation"))
(eval-after-load "magit"
  '(progn
     (setq magit-repository-directories
           (list
            '("~/.emacs.d" . 0)
            '("~/Admin" . 0 )
            '("~/DecorCarmeli/Web2" . 0)
            '("~/Work/Liturgy" . 0)
            '("~/Work/MailingList" . 0)
            '("~/Work/AuthorizeNet" . 0)
            '("~/Programs/Gregorio-Project" . 1)))))

(straight-use-package 'visual-regexp)
(define-key global-map
    (vector 'remap 'query-replace-regexp) 'vr/query-replace)

(straight-use-package
 '(gregorio-mode :type git :host github :repo "eschwab/gregorio-mode.el"
            :upstream (:host github
                       :repo "jsrjenkins/gregorio-mode")))
(eval-after-load "gregorio-mode"
  '(progn
     (set-face-attribute 'gregorio-text nil :inherit 'default)
     (set-face-attribute 'gregorio-accented nil :inherit 'default)
))

(straight-use-package 'auctex)
(setq TeX-auto-save t
      Tex-parse-self t)
(setq-default Tex-master nil)

(eval-after-load "tex"
  '(progn
     (add-to-list 'TeX-command-list
		  '("Lualatex" "lualatex --shell-escape %t"
		    TeX-run-TeX nil (latex-mode)
		    :help "Run Lualatex"))
     (add-to-list 'TeX-command-list
		  '("Biber" "biber %s"
		    TeX-run-discard-foreground nil (latex-mode)
		    :help "Run Biber"))
     (setq font-latex-match-function-keywords
	   '(("incipit" "{{")
	     ("rubric" "{")
	     ("rubricsmall" "{")
	     ("vbar")
	     ("rbar")
	     ("thelord")
	     ("grestar")
	     ("grestarred")
	     ("gredagger")
	     ("gredaggerred")
	     ("OneOne" "{")
	     ("OneTwo" "[{")
	     ("OneThree" "[{")
	     ("TwoOne" "{")
	     ("TwoTwo" "[{")
	     ("TwoThree" "[{")
	     ))
     (setq font-latex-match-reference-keywords
	   '(
	     ("psalminput" "[{")
	     ))
     ))

(setenv "PATH"
        (concat (getenv "PATH")
                ":/usr/local/texlive/2022/bin/x86_64-linux")
)
(setq exec-path
      (append exec-path '("/usr/local/texlive/2022/bin/x86_64-linux"))
)

(defun LaTeX-wordcount (&optional arg)
  "Runs an external perl script to count words in a TeX
file. Print output in the echo area. Wtih a prefix arg, prints
full output in a temporary buffer."
  (interactive "P")
  (if (buffer-modified-p)
      (if (y-or-n-p "Save Buffer?") (save-buffer)
        (error "Aborting: Buffer modified")))
  (if arg (with-output-to-temp-buffer "*LaTeX-Word-Count*"
            (shell-command (concat
                            "/home/elijah/Programs/TeXcount/texcount.pl "
                            (buffer-file-name)) "*LaTeX-Word-Count*"))
    (shell-command (concat "/home/elijah/Programs/TeXcount/texcount.pl "
                           "-brief "
                           (buffer-file-name)))))

(straight-use-package 'org)
(straight-use-package 'org-contrib)
(straight-use-package 'htmlize)
(require 'org)
(global-set-key "\C-cl" 'org-store-link)
(setq org-enforce-todo-dependencies t)

(delete '("\\.pdf\\'" . default) org-file-apps)
(add-to-list 'org-file-apps '(("\\.pdf\\'" . "evince %s")))

(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-ca" 'org-agenda)
(setq org-agenda-start-on-weekday 0)

(defun frogfoot-org-config ()
  "Sets proper org configuration based upon system-name.
For use on 'frogfoot' box."
  (setq org-capture-templates
        '(("o" "Research-Org" entry
           (file+headline "~/Work/Research.org" "Org") "* %?")
          ("t" "Research-TODO" entry
           (file+headline "~/Work/Research.org" "Tasks") "* TODO %? %u")
          ("h" "School-Homework-TODO" entry
           (file+headline "~/School/18th/SchoolList.org" "Assignments")
           "* TODO %? %^g\nDEADLINE: %^{DEADLINE:}t\nAdded: %u")
          ("n" "School-Notes" entry
           (file+headline "~/School/18th/SchoolList.org" "Notes")
           "* %? %u")
          ("v" "Canon Law - Vocabulary" entry
           (file+headline "~/School/18th/CanonLaw/VocabularyList.org"
                          "Vocabulary List") "** %?")
          ))
  (setq org-agenda-files '("~/Work/" "~/Work/Calendar"
                           "~/School/Calendar/" "~/School/18th/")
        org-deadline-warning-days 7)
)

(defun badger-org-config ()
  "Sets proper org configuration based upon system-name.
For use on 'badger' box."
  (setq org-capture-templates
        '(("n" "Projects-Notes" entry
           (file+headline "~/Work/etc/Projects.org" "Project Notes") "* %?")
          ("p" "Projects-TODO" entry
           (file+headline "~/Work/etc/Projects.org" "Current Projects")
           "* TODO %?\nStarted: %u")
          ("g" "General-Notes" entry
           (file+headline "~/Work/etc/Projects.org" "General Notes") "* %?")
          ))
  (setq org-agenda-files '("~/Work/etc/"))
)

(if (string= system-name "frogfoot") (frogfoot-org-config)
  (if (string= system-name "frogfoot.Home") (frogfoot-org-config)
    (if (string= system-name "badger") (badger-org-config))))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (latex . t)
   (shell . t)
   (emacs-lisp . t)
))

(setq org-latex-pdf-process
  '("lualatex -interaction nonstopmode -shell-escape -output-directory %o %f"
    "lualatex -interaction nonstopmode -shell-escape -output-directory %o %f"
    "lualatex -interaction nonstopmode -shell-escape -output-directory %o %f"))

;; Old value before adding support for greek text.
;; (setq org-latex-pdf-process
;;   '("pdflatex -interaction nonstopmode -shell-escape -output-directory %o %f"
;;     "pdflatex -interaction nonstopmode -shell-escape -output-directory %o %f"
;;     "pdflatex -interaction nonstopmode -shell-escape -output-directory %o %f"))

(setq org-latex-listings 'minted)
(setq org-latex-minted-options
           '(("frame" "lines")
             ("fontsize" "\\small")
             ("linenos" "true")))

(setq org-latex-classes
      (list '("article"
              "\\documentclass{article}"
              ("\\section{%s}" . "\\section*{%s}")
              ("\\subsection{%s}" . "\\subsection*{%s}")
              ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
              ("\\paragraph{%s}" . "\\paragraph*{%s}")
              ("\\subparagraph{%s}" . "\\subparagraph*{%s}")
)))

(add-to-list 'org-latex-classes
             '("report"
               "\\documentclass[11pt]{report}"
               ("\\part{%s}" . "\\part*{%s}")
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
))

(add-to-list 'org-latex-classes
             '("book"
               "\\documentclass[11pt]{book}"
               ("\\part{%s}" . "\\part*{%s}")
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
))

(add-to-list 'org-latex-classes
 '("talks"
 "\\documentclass[11pt]{article}
 \\usepackage[AUTO]{inputenc}
 \\usepackage[T1]{fontenc}
 \\usepackage[letterpaper,margin={1in}]{geometry}
 \\usepackage{times}
 \\usepackage{xcolor}
 \\usepackage{longtable}
 \\usepackage{float}
 \\usepackage{wrapfig}
 \\usepackage{hyperref}
 \\usepackage{minted}
 \\usemintedstyle{emacs}
 \\tolerance=1000
 \\setlength{\\parindent}{0mm}
 \\setlength{\\parskip}{4pt}
 [NO-DEFAULT-PACKAGES]
 [NO-PACKAGES]"
 ("\\section{%s}" . "\\section*{%s}")
 ;; ("\\subsection{%s}" . "\\subsection*{%s}")
))

(add-to-list 'org-latex-classes
 '("class-notes"
 "\\documentclass[11pt]{article}
 \\usepackage[letterpaper,margin={20mm}]{geometry}
 \\usepackage[utf8]{inputenc}
 \\usepackage{fontspec}
 \\setmainfont{DejaVu Serif}
 \\usepackage{cjhebrew}
 \\usepackage{longtable}
 \\usepackage{xcolor}
 \\usepackage{float}
 \\usepackage[normalem]{ulem}
 \\usepackage{wrapfig}
 \\usepackage{hyperref}
 \\usepackage{minted}
 \\tolerance=1000
 \\setlength{\\parindent}{0mm}
 \\setlength{\\parskip}{1pt}
 [NO-DEFAULT-PACKAGES]
 [NO-PACKAGES]"
 ("{\\begin{center}\\LARGE\\textbf{%s}\\end{center}}" . "{\\begin{center}\\LARGE\\textbf{%s}\\end{center}}")
 ("\\section{%s}" . "\\section*{%s}")
))

;; Old value
;; (add-to-list 'org-latex-classes
;;  '("class-notes"
;;  "\\documentclass[11pt]{article}
;;  \\usepackage[AUTO]{inputenc}
;;  \\usepackage[T1]{fontenc}
;;  \\usepackage[letterpaper,margin={20mm}]{geometry}
;;  \\usepackage[scaled]{beraserif}
;;  \\usepackage[scaled]{berasans}
;;  \\usepackage[scaled]{beramono}
;;  \\usepackage{xcolor}
;;  \\usepackage{longtable}
;;  \\usepackage{float}
;;  \\usepackage{cjhebrew}
;;  \\usepackage[normalem]{ulem}
;;  \\usepackage{wrapfig}
;;  \\usepackage{hyperref}
;;  \\usepackage{minted}
;;  \\tolerance=1000
;;  \\setlength{\\parindent}{0mm}
;;  \\setlength{\\parskip}{1pt}
;;  [NO-DEFAULT-PACKAGES]
;;  [NO-PACKAGES]"
;;  ("{\\begin{center}\\LARGE\\textbf{%s}\\end{center}}" . "{\\begin{center}\\LARGE\\textbf{%s}\\end{center}}")
;;  ("\\section{%s}" . "\\section*{%s}")
;; ))

(add-to-list 'org-latex-classes
 '("papers"
 "\\documentclass[12pt]{article}
 \\usepackage[AUTO]{inputenc}
 \\usepackage[T1]{fontenc}
 \\usepackage[letterpaper,margin={1in}]{geometry}
 \\usepackage{times}
 \\usepackage{xcolor}
 \\usepackage{longtable}
 \\usepackage{float}
 \\usepackage{cjhebrew}
 \\usepackage{wrapfig}
 \\usepackage{hyperref}
 \\usepackage{minted}
 \\usemintedstyle{emacs}
 \\tolerance=1000
 \\setlength{\\parindent}{0.5in}
 \\setlength{\\parskip}{4pt}
 \\linespread{1.6}
 [NO-DEFAULT-PACKAGES]
 [NO-PACKAGES]"
 ("\\label{%s}" . "\\label{%s}")
 ;; ("\\subsection{%s}" . "\\subsection*{%s}")
))

(straight-use-package 'org-bullets)
(eval-after-load "org-bullets"
  '(progn
     (setq org-bullets-bullet-list '("✠" "◆" "✼" "◉" "○" "✈"))
))

(straight-use-package 'fill-column-indicator)
(eval-after-load "fill-column-indicator"
  '(progn
     (setq-default fci-rule-column 78
                   fci-rule-color "#799b9f")
))

(add-to-list 'auto-mode-alist '("\\.lua\\'" . lua-mode))
(straight-use-package 'lua-mode)
(eval-after-load "lua-mode"
  '(progn
     (setq-default lua-indent-level 2)))

(autoload 'pianobar "pianobar" nil t)

(straight-use-package 'company)

(straight-use-package 'php-mode)

(defun prog-mode-hook-fun ()
  "Function to run with prog-mode-hook."
  (display-line-numbers-mode t)
  (set-face-foreground 'line-number "#98c2c7")
  (fci-mode))

(add-hook 'prog-mode-hook 'prog-mode-hook-fun)

(defun emacs-lisp-mode-hook-fun ()
  "Function to run with emacs-lisp-mode-hook."
  (company-mode))

(add-hook 'emacs-lisp-mode-hook 'emacs-lisp-mode-hook-fun)

(defun ibuffer-mode-hook-fun ()
  "Function to run with ibuffer-mode-hook."
  (ibuffer-switch-to-saved-filter-groups "first")
  (ibuffer-auto-mode 1))

(add-hook 'ibuffer-mode-hook 'ibuffer-mode-hook-fun)

(defun org-mode-hook-fun ()
  "Function to run with org-mode-hook."
  (org-indent-mode t)
  (org-bullets-mode 1))

(add-hook 'org-mode-hook 'org-mode-hook-fun)

(defun python-mode-hook-fun ()
  "Function to run with python-mode-hook."
  (setq python-indent 4)
  (setq tab-width 4)
  (setq tab-stop-list '(4 8 12 16 20 24 28 32 36 40 44 48 52 56 60 64 68))
  (setq indent-tabs-mode nil))

(add-hook 'python-mode-hook 'python-mode-hook-fun)

(defun c-mode-hook-fun ()
  "Function to run with c-mode-hook."
  (setq-default c-basic-offset 4
                tab-width 4
                indent-tabs-mode nil)
  (c-set-offset 'statement-block-intro '+))

(add-hook 'c-mode-hook 'c-mode-hook-fun)

(defun LaTeX-mode-hook-fun ()
  "Function to run with LaTeX-mode-hook."
  (define-key LaTeX-mode-map (kbd "C-c w") 'LaTeX-wordcount)
  (display-line-numbers-mode t)
  (set-face-foreground 'line-number "#98c2c7")
  (visual-line-mode t)
  (TeX-PDF-mode t)
  (reftex-mode t)
  (setq TeX-command-extra-options "-shell-escape"))

(add-hook 'LaTeX-mode-hook 'LaTeX-mode-hook-fun)

(defun after-make-frame-functions-fun (frame)
  "Function to run with after-make-frame-functions (a hook)."
  (set-frame-parameter frame 'font "Monospace-10"))

(defun after-make-fram-functions-fun2 (frame)
  "Function to run with after-make-frame-functions (a hook)."
  (with-current-buffer (get-buffer " *Echo Area 0*")
                        (setq-local face-remapping-alist
                                    '((default (:foreground "#ffcb48")))))
)

(add-hook 'after-make-frame-functions 'after-make-frame-functions-fun t)
;(add-hook 'after-make-frame-functions 'after-make-frame-functions-fun2 t)

(defun find-file-hook-fun ()
  "Tests to see if a buffer is visiting a file with root
privileges. If so, alert the user via header-line."
  (if (string-match "^/su:root" default-directory)
      (setq header-line-format
            "   ** Caution: Editing with ROOT privileges **")
    nil))

(add-hook 'find-file-hook 'find-file-hook-fun t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-safe-themes (quote ("1f2b1f771fb60d7fb0f31d7adfc1e1c4524c7318c3aeb100ad6fab4dce389c0b" default))))

)
(run-with-idle-timer
 5 nil
 (lambda ()
   (setq gc-cons-threshold 800000)
   (message "gc-cons-threshold restored to %S"
            gc-cons-threshold)))
