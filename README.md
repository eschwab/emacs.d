# Emacs Config

An emacs config repository for ~/.emacs.d/ No other repositories are
required Since migrating to use straight.el that package automatically
manages the necessary dependancies for this emacs
configuration. Simply clone and start emacs.

IMPORTANT: straight.el does not properly initialize AUCTeX. Run the following in straight/repos/auctex:

./autogen.sh && ./configure && make

## Files
---------

### init.el
The init file emacs looks for, produced by tangleing `init.org`

### init.org
The real init file for emacs in org for ease of use.

### init.html
A nice html export for fancy viewing/documentation.

### stylesheet.css
CSS for `init.html`
